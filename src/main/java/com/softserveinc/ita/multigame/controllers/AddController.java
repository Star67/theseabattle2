package com.softserveinc.ita.multigame.controllers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.softserveinc.ita.multigame.model.seabattle.Player;
import com.softserveinc.ita.multigame.model.seabattle.SeaBattle;
import com.softserveinc.ita.multigame.services.CrudService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/add")
public class AddController extends HttpServlet {
    CrudService crudService = new CrudService();
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String field = req.getParameter("field");
        field = field.replaceAll("\\[", "").replaceAll("\\]", "").replaceAll(",", "").replaceAll("\"", "").trim();
        char[][] currentField = new char[10][10];
        int k=0;
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                currentField[i][j] = field.charAt(k);
                k++;
            }
        }

        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        String json;

        String turn = req.getParameter("ship");

        SeaBattle game = crudService.getGame(Long.valueOf(req.getParameter("id")));

        if(!game.validateTurnSyntax(turn)){
            json = gson.toJson("Wrong syntax");
            resp.getWriter().write(json);
        }
        else{
            int x = turn.charAt(0) - 97;
            int y;
            if(turn.length()==2)
                y = turn.charAt(1)-48-1;
            else y=9;

            currentField[x][y] = '#';

            json = gson.toJson(currentField);
            resp.getWriter().write(json);
        }

    }
}
