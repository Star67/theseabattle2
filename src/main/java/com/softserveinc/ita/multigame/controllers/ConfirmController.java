package com.softserveinc.ita.multigame.controllers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.softserveinc.ita.multigame.model.seabattle.Player;
import com.softserveinc.ita.multigame.model.seabattle.SeaBattle;
import com.softserveinc.ita.multigame.services.ConfirmFieldService;
import com.softserveinc.ita.multigame.services.CrudService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/confirm")
public class ConfirmController extends HttpServlet {
    CrudService crudService = new CrudService();
    ConfirmFieldService confirmFieldService = new ConfirmFieldService();
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Long id = Long.parseLong(req.getParameter("id"));
        SeaBattle game = crudService.getGame(id);
        Player player = (Player) req.getSession().getAttribute("player");

        String field = req.getParameter("field");
        field = field.replaceAll("\\[", "").replaceAll("\\]", "").replaceAll(",", "").replaceAll("\"", "").trim();

        char[][] currentField = new char[10][10];
        int k=0;
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                currentField[i][j] = field.charAt(k);
                k++;
            }
        }
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");

        String json;

        if(player.getLogin().equals(game.getFirstPlayer())){
            if(confirmFieldService.confirmField(currentField)) {
                game.setFirstPlayerField(currentField);
                json = gson.toJson("field is setted");
            }
            else{
                json = gson.toJson("field is not setted");
            }


        }
        else {
            if(confirmFieldService.confirmField(currentField)){
                game.setSecondPlayer(player.getLogin());
                game.setSecondPlayerField(currentField);
                json = gson.toJson("field is setted");
            }
            else{
                json = gson.toJson("field is not setted");
            }

        }

        resp.getWriter().write(json);

    }
}
