package com.softserveinc.ita.multigame.controllers;

import com.softserveinc.ita.multigame.model.seabattle.Player;
import com.softserveinc.ita.multigame.model.seabattle.SeaBattle;
import com.softserveinc.ita.multigame.services.CrudService;
import com.softserveinc.ita.multigame.services.PlayerService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/create")
public class CreateGameController extends HttpServlet{
    CrudService crudService = new CrudService();
    PlayerService playerService = new PlayerService();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Player player = playerService.get(((Player) req.getSession().getAttribute("player")).getLogin());
        SeaBattle game = crudService.createGame(player, true);
        Long id = game.getId();

        req.setAttribute("id", id);

        req.getRequestDispatcher("WEB-INF/views/setships_2.jsp").forward(req, resp);
    }
}
