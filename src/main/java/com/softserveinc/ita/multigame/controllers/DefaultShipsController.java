package com.softserveinc.ita.multigame.controllers;

import com.softserveinc.ita.multigame.model.seabattle.Player;
import com.softserveinc.ita.multigame.model.seabattle.SeaBattle;
import com.softserveinc.ita.multigame.services.CrudService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/default")
public class DefaultShipsController extends HttpServlet{
    CrudService crudService = new CrudService();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Long id = Long.valueOf(req.getParameter("id"));
        SeaBattle game = crudService.getGame(id);
        Player player = (Player) req.getSession().getAttribute("player");

        if(player.getLogin().equals(game.getFirstPlayer())){
            game.initFirstFieldDefault();
            resp.sendRedirect(req.getContextPath() + "/login");
        }
        else{
            game.setSecondPlayer(player.getLogin());
            game.initSecondFieldDefault();
            resp.sendRedirect(req.getContextPath() + "/login");
        }
    }
}
