package com.softserveinc.ita.multigame.controllers;

import com.softserveinc.ita.multigame.model.seabattle.SeaBattle;
import com.softserveinc.ita.multigame.services.CrudService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/info")
public class GameInfoController extends HttpServlet{
    CrudService crudService = new CrudService();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Long id = Long.valueOf(req.getParameter("id"));
        SeaBattle game = crudService.getGame(id);

        if(game.isFinished()) {
            resp.sendRedirect(req.getContextPath() + "/result?id=" + id);
        }else {
            String firstPlayer = game.getFirstPlayer();
            String secondPlayer = game.getSecondPlayer();
            req.setAttribute("firstPlayer", firstPlayer);
            req.setAttribute("secondPlayer", secondPlayer);

            req.getRequestDispatcher("WEB-INF/views/gameinfo_2.jsp").forward(req, resp);
        }
    }
}
