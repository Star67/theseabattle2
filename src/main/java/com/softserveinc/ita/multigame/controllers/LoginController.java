package com.softserveinc.ita.multigame.controllers;

import com.softserveinc.ita.multigame.model.seabattle.Player;
import com.softserveinc.ita.multigame.services.PlayerService;
import com.softserveinc.ita.multigame.services.ShowGamesService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@WebServlet("/login")
public class LoginController extends HttpServlet{

    ShowGamesService showGamesService = new ShowGamesService();
    PlayerService playerService = new PlayerService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String playerName = req.getRemoteUser();
        Player player = playerService.get(playerName);

        List<Long> createdGames = showGamesService.getCreatedGamesIds(player);
        List<Long> currentGames = showGamesService.getCurrentGamesIds(player);
        List<Long> waitingGames = showGamesService.getWaitingGamesIds(player);

        /******Place Player into the Session******/
        HttpSession session = req.getSession();
        session.setMaxInactiveInterval(20*60);
        session.setAttribute("player", player);

        req.setAttribute("createdGames", createdGames);
        req.setAttribute("currentGames", currentGames);
        req.setAttribute("waitingGames", waitingGames);

        req.getRequestDispatcher("WEB-INF/views/gamelist.jsp").forward(req,resp);

    }
}
