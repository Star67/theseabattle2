package com.softserveinc.ita.multigame.controllers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.softserveinc.ita.multigame.model.seabattle.Player;
import com.softserveinc.ita.multigame.model.seabattle.SeaBattle;
import com.softserveinc.ita.multigame.services.CrudService;
import com.softserveinc.ita.multigame.services.MakeTurnService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/turn")
public class MakeTurnController extends HttpServlet {
    MakeTurnService makeTurnService = new MakeTurnService();
    CrudService crudService = new CrudService();
    SeaBattle game;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Long id = Long.valueOf(req.getParameter("id"));
        Player player = (Player) req.getSession().getAttribute("player");
        String turn = req.getParameter("turn");

        game = crudService.getGame(id);
        makeTurnService.makeTurn(id, player.getLogin(), turn);

        int code = game.getResultCode();

        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        char[][] currentPlayerFieldToHit;

        if (player.getLogin().equals(game.getFirstPlayer())) {
            currentPlayerFieldToHit = game.getShowStatFirst();
        } else {
            currentPlayerFieldToHit = game.getShowStatSecond();
        }

        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");

        String json;

        if(game.isFinished()) {
            json = gson.toJson("Game Over");
            resp.getWriter().write(json);
        }
        else if(code != 0){
            json = gson.toJson(code);
            resp.getWriter().write(json);
        }
        else{
            json = gson.toJson(currentPlayerFieldToHit);
            resp.getWriter().write(json);
        }
    }
}
