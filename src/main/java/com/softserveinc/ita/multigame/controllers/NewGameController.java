package com.softserveinc.ita.multigame.controllers;

import com.softserveinc.ita.multigame.model.seabattle.Player;
import com.softserveinc.ita.multigame.services.CrudService;
import com.softserveinc.ita.multigame.services.PlayerService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/new")
public class NewGameController extends HttpServlet{
    CrudService crudService = new CrudService();
    PlayerService playerService = new PlayerService();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Player player = playerService.get(((Player) req.getSession().getAttribute("player")).getLogin());
        crudService.createGame(player);
        resp.sendRedirect(req.getContextPath() + "/login");
    }
}
