package com.softserveinc.ita.multigame.controllers;

import com.softserveinc.ita.multigame.dao.PlayerDao;
import com.softserveinc.ita.multigame.dao.PlayerDaoHiberImpl;
import com.softserveinc.ita.multigame.model.seabattle.Player;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/players")
public class PlayerListController extends HttpServlet{
    PlayerDao playerDao = new PlayerDaoHiberImpl();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        List<Player> playerList = playerDao.getAll();

        req.setAttribute("playerList", playerList);

        req.getRequestDispatcher("WEB-INF/views/player_list.jsp").forward(req, resp);
    }
}
