package com.softserveinc.ita.multigame.controllers;

import com.softserveinc.ita.multigame.dao.PlayerDao;
import com.softserveinc.ita.multigame.dao.PlayerDaoHiberImpl;
import com.softserveinc.ita.multigame.model.seabattle.Player;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/profile")
public class PlayerProfileController extends HttpServlet {
    PlayerDao playerDao = new PlayerDaoHiberImpl();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String playerName = ((Player)req.getSession().getAttribute("player")).getLogin();
        Player player = playerDao.get(playerName);

        req.setAttribute("email", player.getEmail());
        req.setAttribute("password", player.getPassword());

        req.getRequestDispatcher("WEB-INF/views/profile.jsp").forward(req, resp);
    }
}
