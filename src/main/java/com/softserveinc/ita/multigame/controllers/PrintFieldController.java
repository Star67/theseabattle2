package com.softserveinc.ita.multigame.controllers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/print")
public class PrintFieldController extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        char [][] fieldForSetttings = new char[][]{         {'_','_','_', '_', '_','_','_', '_', '_', '_'},
                                                            {'_','_','_', '_', '_','_','_', '_', '_', '_'},
                                                            {'_','_','_', '_', '_','_','_', '_', '_', '_'},
                                                            {'_','_','_', '_', '_','_','_', '_', '_', '_'},
                                                            {'_','_','_', '_', '_','_','_', '_', '_', '_'},
                                                            {'_','_','_', '_', '_','_','_', '_', '_', '_'},
                                                            {'_','_','_', '_', '_','_','_', '_', '_', '_'},
                                                            {'_','_','_', '_', '_','_','_', '_', '_', '_'},
                                                            {'_','_','_', '_', '_','_','_', '_', '_', '_'},
                                                            {'_','_','_', '_', '_','_','_', '_', '_', '_'}
                                                    };
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        String json = gson.toJson(fieldForSetttings);
        resp.getWriter().write(json);

    }
}
