package com.softserveinc.ita.multigame.controllers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.softserveinc.ita.multigame.model.seabattle.Player;

import com.softserveinc.ita.multigame.model.seabattle.SeaBattle;
import com.softserveinc.ita.multigame.services.CrudService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/refresh")
public class RefreshFieldController extends HttpServlet{
    CrudService crudService = new CrudService();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

            Long id = Long.valueOf(req.getParameter("id"));
            Player player = (Player) req.getSession().getAttribute("player");
            SeaBattle game = crudService.getGame(id);
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            char[][] currentPlayerField;

            if (player.getLogin().equals(game.getFirstPlayer())) {
                currentPlayerField = game.getFirstPlayerField();
            } else {
                currentPlayerField = game.getSecondPlayerField();
            }

            resp.setContentType("application/json");
            resp.setCharacterEncoding("UTF-8");

            String json;

            if(game.isFinished()) {
                json = gson.toJson("Game Over");
                resp.getWriter().write(json);
            }
            else{
                json = gson.toJson(currentPlayerField);
                resp.getWriter().write(json);
            }
}
}