package com.softserveinc.ita.multigame.controllers;

import com.softserveinc.ita.multigame.model.seabattle.Player;
import com.softserveinc.ita.multigame.services.PlayerService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/register")
public class RegistrationController extends HttpServlet {
    PlayerService playerService = new PlayerService();
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String login = req.getParameter("login");
        String email = req.getParameter("email");
        String password = req.getParameter("password");
        Player player = new Player(login, password, email);

        int code = playerService.create(player);

        if(code == 0){
            resp.getWriter().print("<h1>Seems like this player already exists...</h1>");
        }else{
            playerService.addRole(player, "players");
            resp.sendRedirect(req.getContextPath()+"/login");
        }

    }
}
