package com.softserveinc.ita.multigame.controllers;

import com.softserveinc.ita.multigame.model.seabattle.Player;
import com.softserveinc.ita.multigame.model.seabattle.SeaBattle;
import com.softserveinc.ita.multigame.services.CrudService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/result")
public class ResultController extends HttpServlet {
    CrudService crudService = new CrudService();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Long id = Long.parseLong(req.getParameter("id"));
        Player player = (Player) req.getSession().getAttribute("player");

        SeaBattle game = crudService.getGame(id);

        if(player.getLogin().equals(game.getTheWinner())){
            req.getRequestDispatcher("WEB-INF/views/winpage.jsp").forward(req, resp);
        }
        else{
            req.getRequestDispatcher("WEB-INF/views/losepage.jsp").forward(req, resp);
        }
    }
}