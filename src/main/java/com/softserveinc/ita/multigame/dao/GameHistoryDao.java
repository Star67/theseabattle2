package com.softserveinc.ita.multigame.dao;

import com.softserveinc.ita.multigame.model.seabattle.GameHistory;
import java.util.List;

public interface GameHistoryDao {
    GameHistory get(Long id);
    void create(GameHistory gameHistory);
    void update(GameHistory gameHistory);
    void delete(GameHistory gameHistory);
    List<GameHistory> getAll();
}
