package com.softserveinc.ita.multigame.dao;

import com.softserveinc.ita.multigame.dao.utils.HibernateSessionFactory;
import com.softserveinc.ita.multigame.model.seabattle.GameHistory;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.List;

public class GameHistoryHiberImpl implements GameHistoryDao{

    SessionFactory sessionFactory = HibernateSessionFactory.SESSION_FACTORY.getSessionFactory();
    private Session session;

    @Override
    public GameHistory get(Long id) {
        GameHistory gameHistory;
        session = sessionFactory.openSession();
        session.beginTransaction();
        gameHistory = session.get(GameHistory.class, id);
        session.getTransaction().commit();
        session.close();
        return gameHistory;
    }

    @Override
    public void create(GameHistory gameHistory) {
        session = sessionFactory.openSession();
        session.beginTransaction();
        session.save(gameHistory);
        session.getTransaction().commit();
        session.close();
    }

    @Override
    public void update(GameHistory gameHistory) {
        session = sessionFactory.openSession();
        session.beginTransaction();
        session.update(gameHistory);
        session.getTransaction().commit();
        session.close();
    }

    @Override
    public void delete(GameHistory gameHistory) {
        session = sessionFactory.openSession();
        session.beginTransaction();
        session.delete(gameHistory);
        session.getTransaction().commit();
        session.close();
    }

    @Override
    public List<GameHistory> getAll() {
        List<GameHistory> gameHistories;
        session = sessionFactory.openSession();
        session.beginTransaction();
        gameHistories = session.createQuery("from GameHistory").list();
        session.getTransaction().commit();
        session.close();
        return gameHistories;
    }
}
