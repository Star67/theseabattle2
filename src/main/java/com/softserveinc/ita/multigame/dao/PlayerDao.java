package com.softserveinc.ita.multigame.dao;


import com.softserveinc.ita.multigame.model.seabattle.Player;
import java.util.List;

public interface PlayerDao {
    void save(Player player);
    void update(Player player);
    void delete(Player player);
    void delete(Long id);
    Player get(Long playerId);
    Player get(String login);
    List<Player> getAll();
}
