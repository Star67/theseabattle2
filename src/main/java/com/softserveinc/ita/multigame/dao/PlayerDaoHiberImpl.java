package com.softserveinc.ita.multigame.dao;

import com.softserveinc.ita.multigame.dao.utils.HibernateSessionFactory;
import com.softserveinc.ita.multigame.model.seabattle.Player;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import java.util.List;

public class PlayerDaoHiberImpl implements PlayerDao{

    SessionFactory sessionFactory = HibernateSessionFactory.SESSION_FACTORY.getSessionFactory();
    private Session session;

    @Override
    public void save(Player player) {
        session = sessionFactory.openSession();
        session.beginTransaction();
        session.save(player);
        session.getTransaction().commit();
        session.close();
    }

    @Override
    public void update(Player player) {
        session = sessionFactory.openSession();
        session.beginTransaction();
        session.update(player);
        session.getTransaction().commit();
        session.close();
    }

    @Override
    public void delete(Player player) {
        session = sessionFactory.openSession();
        session.beginTransaction();
        session.delete(player);
        session.getTransaction().commit();
        session.close();
    }

    @Override
    public void delete(Long id) {
        Player player;
        session = sessionFactory.openSession();
        session.beginTransaction();
        player = session.load(Player.class, id);
        session.delete(player);
        session.getTransaction().commit();
        session.close();
    }

    @Override
    public Player get(Long playerId) {
        Player player;
        session = sessionFactory.openSession();
        session.beginTransaction();
        player = session.get(Player.class, playerId);
        session.getTransaction().commit();
        session.close();
        return player;
    }

    @Override
    public Player get(String login) {
        Player player;
        session = sessionFactory.openSession();
        session.beginTransaction();
        Query query =  session.createQuery("from Player p where p.login = :login");
        query.setParameter("login", login);
        player = (Player) query.uniqueResult();
        session.getTransaction().commit();
        session.close();
        return player;
    }

    @Override
    public List<Player> getAll() {
        List<Player> players;
        session = sessionFactory.openSession();
        session.beginTransaction();
        players = session.createQuery("from Player").list();
        session.getTransaction().commit();
        session.close();
        return players;
    }
}
