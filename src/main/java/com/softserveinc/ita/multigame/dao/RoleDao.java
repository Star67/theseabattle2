package com.softserveinc.ita.multigame.dao;

import com.softserveinc.ita.multigame.model.seabattle.Player;

public interface RoleDao {
    void addRole(Player player, String role);
    void deleteRole(Long id);
}
