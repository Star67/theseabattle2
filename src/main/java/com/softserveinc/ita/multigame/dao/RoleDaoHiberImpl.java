package com.softserveinc.ita.multigame.dao;

import com.softserveinc.ita.multigame.dao.utils.HibernateSessionFactory;
import com.softserveinc.ita.multigame.model.seabattle.Player;
import com.softserveinc.ita.multigame.model.seabattle.Role;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class RoleDaoHiberImpl implements RoleDao{
    SessionFactory sessionFactory = HibernateSessionFactory.SESSION_FACTORY.getSessionFactory();
    Session session;
    public void addRole(Player player, String role){
        session = sessionFactory.openSession();
        session.beginTransaction();
        Role r = new Role();
        r.setLogin(player.getLogin());
        r.setRole(role);
        session.save(r);
        session.getTransaction().commit();
        session.close();
    }
    public void deleteRole(Long id){
        session = sessionFactory.openSession();
        session.beginTransaction();
        Role role = session.get(Role.class, id);
        session.delete(role);
        session.getTransaction().commit();
        session.close();
    }
}
