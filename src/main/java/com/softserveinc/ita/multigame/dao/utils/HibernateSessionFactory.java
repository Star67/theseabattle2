package com.softserveinc.ita.multigame.dao.utils;


import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public enum HibernateSessionFactory {
    SESSION_FACTORY;

    private SessionFactory sessionFactory;

    HibernateSessionFactory(){
        sessionFactory = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();
    }

    public SessionFactory getSessionFactory(){
        return sessionFactory;
    }
}
