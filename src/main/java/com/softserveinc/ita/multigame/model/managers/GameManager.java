package com.softserveinc.ita.multigame.model.managers;

import com.softserveinc.ita.multigame.model.seabattle.Player;
import com.softserveinc.ita.multigame.model.seabattle.SeaBattle;

import java.util.List;

public interface GameManager {
    SeaBattle getGame(Long id);
    List<Long> getCreatedGamesIds(Player player);
    List<Long> getCurrentGamesIds(Player player);
    List<Long> getWaitingGamesIds(Player player);
    boolean createGame(Player player);
    SeaBattle createGame(Player player, boolean customGame);
    boolean deleteGame(Long id);
}
