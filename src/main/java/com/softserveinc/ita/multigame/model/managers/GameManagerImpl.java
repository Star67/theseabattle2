package com.softserveinc.ita.multigame.model.managers;


import com.softserveinc.ita.multigame.dao.PlayerDao;
import com.softserveinc.ita.multigame.dao.PlayerDaoHiberImpl;
import com.softserveinc.ita.multigame.model.seabattle.Player;
import com.softserveinc.ita.multigame.model.seabattle.SeaBattle;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;

public class GameManagerImpl implements GameManager {
    private static List<SeaBattle> gameList = new CopyOnWriteArrayList<SeaBattle>();
    private static GameManager instance = new GameManagerImpl();
    PlayerDao playerDao = new PlayerDaoHiberImpl();
    private GameManagerImpl() {

        SeaBattle bat1 = new SeaBattle();
        SeaBattle bat2 = new SeaBattle();
        SeaBattle bat3 = new SeaBattle();
        SeaBattle bat4 = new SeaBattle();
        SeaBattle bat5 = new SeaBattle();

        bat1.setFirstPlayer(playerDao.get("Vasya").getLogin());
        bat1.setSecondPlayer(playerDao.get("Zhora").getLogin());

        bat2.setFirstPlayer(playerDao.get("Vasya").getLogin());
        bat2.setSecondPlayer(playerDao.get("Petya").getLogin());

        bat3.setFirstPlayer(playerDao.get("Alex").getLogin());
        bat3.setSecondPlayer(playerDao.get("Vasya").getLogin());

        bat4.setFirstPlayer(playerDao.get("Kolya").getLogin());

        bat5.setFirstPlayer(playerDao.get("Vasya").getLogin());

        gameList.add(bat1);
        gameList.add(bat2);
        gameList.add(bat3);
        gameList.add(bat4);
        gameList.add(bat5);
    }

    public static GameManager getInstance() {
        return instance;
    }

    public SeaBattle getGame(Long id) {
        for (SeaBattle game: gameList){
            if( game.getId() == id ){
                return game;
            }
        }
        return null;
    }

    public List<Long> getCreatedGamesIds(Player player) {
        List<Long> createdGamesIds = gameList.stream()
                                                .filter(game -> player.getLogin().equals(game.getFirstPlayer())
                                                        && game.getSecondPlayer() == null).map(SeaBattle::getId)
                                                .collect(Collectors.toList());
        return createdGamesIds;
    }
    public List<Long> getCurrentGamesIds(Player player) {
        List<Long> currentGamesIds = gameList.stream()
                                                .filter(game -> (player.getLogin().equals(game.getFirstPlayer())
                                                        && game.getSecondPlayer() != null) || player.getLogin().equals(game.getSecondPlayer()))
                                                .map(SeaBattle::getId)
                                                .collect(Collectors.toList());
        return currentGamesIds;
    }
    public List<Long> getWaitingGamesIds(Player player) {
        List<Long> waitingGamesIds = gameList.stream()
                                                .filter(game -> game.getSecondPlayer() == null
                                                        && !game.getFirstPlayer().equals(player.getLogin()))
                                                .map(SeaBattle::getId)
                                                .collect(Collectors.toList());
        return waitingGamesIds;
    }

    public boolean createGame(Player player){
        SeaBattle sb = new SeaBattle();
        sb.setFirstPlayer(player.getLogin());
        return gameList.add(sb);
    }

    public SeaBattle createGame(Player player, boolean customGame){
        if(customGame == true){
            SeaBattle sb = new SeaBattle();
            sb.setFirstPlayer(player.getLogin());
            gameList.add(sb);
            return sb;
        }
        else
            createGame(player);
        return null;
    }

    public boolean deleteGame(Long id) {
        return gameList.remove(id);
    }
   
}
