package com.softserveinc.ita.multigame.model.seabattle;

import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@EqualsAndHashCode(of = "seaBattle")
public class Game {
    private SeaBattle seaBattle;
    private Player firstPlayer;
    private Player secondPlayer;
    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private List<String> turns;

    public void setSeaBattle(SeaBattle seaBattle) {
        this.seaBattle = seaBattle;
    }

    public void setFirstPlayer(Player firstPlayer) {
        this.firstPlayer = firstPlayer;
    }

    public void setSecondPlayer(Player secondPlayer) {
        this.secondPlayer = secondPlayer;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    public void setTurns(List<String> turns) {
        this.turns = turns;
    }
}
