package com.softserveinc.ita.multigame.model.seabattle;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@ToString
@EqualsAndHashCode(of = "id")
@Entity
@Table(name = "game_history")
public class GameHistory {

    @Id
    @GeneratedValue
    private Long id;
    @ManyToOne
    @JoinColumn(name = "first_player_id")
    private Player firstPlayer;
    @ManyToOne
    @JoinColumn(name = "second_player_id")
    private Player secondPlayer;
    private LocalDateTime startTime;
    private LocalDateTime endTime;
    @ElementCollection
    private List<String> turnList;
    @Enumerated(EnumType.STRING)
    private GameResult gameResult;

    public GameHistory() {
    }
}
