package com.softserveinc.ita.multigame.model.seabattle;

public enum GameResult {
    FIRST_IS_WINNER, SECOND_IS_WINNER
}
