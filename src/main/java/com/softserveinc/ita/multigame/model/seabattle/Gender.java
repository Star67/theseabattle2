package com.softserveinc.ita.multigame.model.seabattle;

public enum Gender {
    MALE, FEMALE
}