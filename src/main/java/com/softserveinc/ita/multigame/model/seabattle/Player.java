package com.softserveinc.ita.multigame.model.seabattle;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
@ToString
@EqualsAndHashCode(of = "id")
@Entity
@Table(name = "players")
public class Player {
    @Id
    @GeneratedValue
    private Long id;
    @Basic(optional = false)
    private String login;
    @Basic(optional = false)
    private String password;
    @Basic(optional = false)
    private String email;
    private String fullName;
    @Enumerated(EnumType.STRING)
    private Gender gender;
    @Column(name = "birthday_date")
    private LocalDate birthdayDate;
    @Column(name = "registration_time")
    private LocalDateTime registrationTime;
    private String about;
    private String avatar;

    public Player() {}
    public Player(String login) { this.login = login; }
    public Player(String login, String password, String email) {
        this.login = login;
        this.password = password;
        this.email = email;
    }
}
