package com.softserveinc.ita.multigame.model.seabattle;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "roles")
public class Role {
    @Id
    @GeneratedValue
    private Long id;

    private String login;

    private String role;

    public Role() {
    }
}
