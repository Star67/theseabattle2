package com.softserveinc.ita.multigame.model.seabattle;

import com.softserveinc.ita.multigame.model.engine.GameState;
import com.softserveinc.ita.multigame.model.engine.GenericGameEngine;

import java.util.Arrays;
import java.util.function.Function;
import java.util.function.Predicate;

public class SeaBattle extends GenericGameEngine<String>{

    private char [][] firstPlayerField       = new char[10][10];
    private char [][] secondPlayerField      = new char[10][10];
    public char  [][] firstPlayerScoreField  = new char[10][10];
    public char  [][] secondPlayerScoreField = new char[10][10];

    public SeaBattle(){
        initFirstFieldDefault();
        initSecondFieldDefault();
        Arrays
                .stream(firstPlayerScoreField)
                .forEach( (char[] a) -> Arrays.fill(a, '_') );
        Arrays
                .stream(secondPlayerScoreField)
                .forEach( (char[] a) -> Arrays.fill(a, '_') );
    }

    public void initFirstFieldDefault(){
        firstPlayerField = new char[][]{        {'#','_','#', '_', '#','_','#', '_', '_', '_'},
                                                {'#','_','#', '_', '#','_','_', '_', '_', '_'},
                                                {'#','_','#', '_', '_','_','#', '_', '_', '_'},
                                                {'#','_','_', '_', '#','_','_', '_', '_', '_'},
                                                {'_','_','#', '_', '#','_','#', '_', '_', '_'},
                                                {'_','_','#', '_', '_','_','_', '_', '_', '_'},
                                                {'_','_','#', '_', '#','_','#', '_', '_', '_'},
                                                {'_','_','_', '_', '#','_','_', '_', '_', '_'},
                                                {'_','_','_', '_', '_','_','_', '_', '_', '_'},
                                                {'_','_','_', '_', '_','_','_', '_', '_', '_'}
        };
    }
    public void initSecondFieldDefault(){
        secondPlayerField = new char[][]{       {'#','#','#', '#', '_','_','_', '_', '_', '_'},
                                                {'_','_','_', '_', '_','_','_', '_', '_', '_'},
                                                {'#','#','#', '_', '#','#','#', '_', '_', '_'},
                                                {'_','_','_', '_', '_','_','_', '_', '_', '_'},
                                                {'#','#','_', '#', '#','_','#', '#', '_', '_'},
                                                {'_','_','_', '_', '_','_','_', '_', '_', '_'},
                                                {'#','_','#', '_', '#','_','#', '_', '_', '_'},
                                                {'_','_','_', '_', '_','_','_', '_', '_', '_'},
                                                {'_','_','_', '_', '_','_','_', '_', '_', '_'},
                                                {'_','_','_', '_', '_','_','_', '_', '_', '_'}
        };
    }

    private Predicate<String> isLength2    = (str) -> str.length() == 2;
    private Function<String, Integer> getX = (str) -> str.charAt(0) - 97;
    private Function<String, Integer> getY = (str) -> { if(isLength2.test(str))
                                                                 return str.charAt(1)-48-1;
                                                        else     return 9;
                                                      };
    private boolean validatePlayerLogic(int x, int y, char[][]field){
        switch (field[x][y]){
            case 'X': return false;
            case '*': return false;
            default:  return true;
        }

        //TODO: neighbor cells validation
    }

    private boolean isActive() {
        boolean firstPlayerInGame=false;
        boolean secondPlayerInGame=false;

        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                char c1 = firstPlayerField [i][j];
                char c2 = secondPlayerField [i][j];
                if(c1 == '#')
                    secondPlayerInGame=true;
                if(c2 == '#')
                    firstPlayerInGame=true;
            }
        }
        if(!firstPlayerInGame || !secondPlayerInGame){
            if(!firstPlayerInGame)
                gameState = GameState.FINISHED_WITH_FIRST_PLAYER_AS_A_WINNER;
            else
                gameState = GameState.FINISHED_WITH_SECOND_PLAYER_AS_A_WINNER;

        }

        return firstPlayerInGame && secondPlayerInGame;
    }

    public char[][] getFirstPlayerField(){
        return firstPlayerField;
    }
    public char[][] getSecondPlayerField(){
        return secondPlayerField;
    }
    public char[][] getShowStatFirst(){
        return firstPlayerScoreField;
    }
    public char[][] getShowStatSecond(){
        return secondPlayerScoreField;
    }
    public void setFirstPlayerField(char[][] firstPlayerField) {
        this.firstPlayerField = firstPlayerField;
    }
    public void setSecondPlayerField(char[][] secondPlayerField) {
        this.secondPlayerField = secondPlayerField;
    }

    @Override
    public boolean validateTurnSyntax(String turn) {

        Predicate<String> isLength3     = (str) -> str.length() == 3;
        Predicate<String> isValidLetter = (str) -> str.charAt(0)-97 >= 0 && str.charAt(0)-97 <= 9;
        Predicate<String> isValidNumber = (str) -> str.charAt(1)-48 >= 1 && str.charAt(1)-48 <=9;
        Predicate<String> isNumber10    = (str) -> {
                                                        try {
                                                            return Integer.valueOf(str.substring(1)) == 10;
                                                        } catch (NumberFormatException e) {
                                                            return false;
                                                        }
                                                   };
        turn=turn.toLowerCase();
        return  isLength2.and(isValidLetter).and(isValidNumber).test(turn) ||
                isLength3.and(isValidLetter).and(isNumber10).test(turn);
    }

    @Override
    protected boolean validateTurnLogic(String turn) {
        turn=turn.toLowerCase();
        int x = getX.apply(turn);
        int y = getY.apply(turn);

        if(gameState == GameState.WAIT_FOR_FIRST_PLAYER_TURN)
            return validatePlayerLogic(x, y, secondPlayerField);
        else if(gameState == GameState.WAIT_FOR_SECOND_PLAYER_TURN)
            return validatePlayerLogic(x,y, firstPlayerField);
        else return true;
    }

    @Override
    protected boolean validatePlayer(String player) {
        return !(player.equals("")) && !(player == null);
    }

    @Override
    protected int changeGameState(String player, String turn) {
        turn=turn.toLowerCase();
        int x = getX.apply(turn);
        int y = getY.apply(turn);

        if(player.equals(getFirstPlayer())){
            switch (secondPlayerField[x][y]){
                case '_': if(!isActive())
                                return gameState;
                          else{
                                secondPlayerField[x][y] = '*';
                                firstPlayerScoreField[x][y] = '*';
                                return GameState.WAIT_FOR_SECOND_PLAYER_TURN;
                          }
                case '#': secondPlayerField[x][y] = 'X';
                          firstPlayerScoreField[x][y] = 'X';
                          if(!isActive())
                                return gameState;
                          else return GameState.WAIT_FOR_FIRST_PLAYER_TURN;
                case 'X': return GameState.WAIT_FOR_FIRST_PLAYER_TURN;
                case '*': return GameState.WAIT_FOR_FIRST_PLAYER_TURN;
                default:  return GameState.UNDEFINED;
            }
        }
        else {
            switch (firstPlayerField[x][y]){
                case '_': firstPlayerField[x][y] = '*';
                    secondPlayerScoreField[x][y] = '*';
                    return GameState.WAIT_FOR_FIRST_PLAYER_TURN;
                case '#': firstPlayerField[x][y] = 'X';
                          secondPlayerScoreField[x][y] = 'X';
                          if(!isActive())
                              return gameState;
                          else return GameState.WAIT_FOR_SECOND_PLAYER_TURN;
                case 'X': return GameState.WAIT_FOR_SECOND_PLAYER_TURN;
                case '*': return GameState.WAIT_FOR_SECOND_PLAYER_TURN;
                default:  return GameState.UNDEFINED;
            }
        }
    }


}
