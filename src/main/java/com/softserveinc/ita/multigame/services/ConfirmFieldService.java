package com.softserveinc.ita.multigame.services;

import com.softserveinc.ita.multigame.model.managers.GameManager;
import com.softserveinc.ita.multigame.model.managers.GameManagerImpl;

public class ConfirmFieldService {

    private GameManager gameManager = GameManagerImpl.getInstance();

    public boolean confirmField(char [][] field){
        int shipCount = 0;
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                if(field[i][j] == '#')
                    shipCount++;
            }
        }

        return shipCount == 20;
    }
}
