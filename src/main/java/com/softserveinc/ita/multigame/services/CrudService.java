package com.softserveinc.ita.multigame.services;

import com.softserveinc.ita.multigame.model.managers.GameManager;
import com.softserveinc.ita.multigame.model.managers.GameManagerImpl;
import com.softserveinc.ita.multigame.model.seabattle.Player;
import com.softserveinc.ita.multigame.model.seabattle.SeaBattle;

public class CrudService {
    private GameManager gameManager = GameManagerImpl.getInstance();

    public boolean createGame(Player firstPlayer){
        return gameManager.createGame(firstPlayer);
    }
    public SeaBattle createGame(Player firstPlayer, boolean customGame){
        return gameManager.createGame(firstPlayer, customGame);
    }
    public SeaBattle getGame(Long id){
        return gameManager.getGame(id);
    }
    public boolean deleteGame(Long id){
        return gameManager.deleteGame(id);
    }
}
