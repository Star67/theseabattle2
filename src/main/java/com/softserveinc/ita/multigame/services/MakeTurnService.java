package com.softserveinc.ita.multigame.services;

import com.softserveinc.ita.multigame.model.managers.GameManager;
import com.softserveinc.ita.multigame.model.managers.GameManagerImpl;

public class MakeTurnService {
    private GameManager gameManager = GameManagerImpl.getInstance();

    public boolean makeTurn(Long id, String playerName, String turn){
        return gameManager.getGame(id).makeTurn(playerName, turn);
    }
}
