package com.softserveinc.ita.multigame.services;

import com.softserveinc.ita.multigame.dao.PlayerDao;
import com.softserveinc.ita.multigame.dao.PlayerDaoHiberImpl;
import com.softserveinc.ita.multigame.dao.RoleDaoHiberImpl;
import com.softserveinc.ita.multigame.model.seabattle.Player;

public class PlayerService {
    PlayerDao playerDao = new PlayerDaoHiberImpl();
    RoleDaoHiberImpl roleDaoHiberImpl = new RoleDaoHiberImpl();

    public int create(Player player){
       try{
            playerDao.save(player);
            return 1;
       }catch(Exception e){
           return 0;
       }
    }
    public int addRole(Player player, String role){
        try{
            roleDaoHiberImpl.addRole(player, role);
            return 1;
        }catch (Exception e){
            return 0;
        }
    }
    public Player get(String login){
        return playerDao.get(login);
    }
}
