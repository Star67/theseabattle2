package com.softserveinc.ita.multigame.services;

import com.softserveinc.ita.multigame.model.managers.GameManager;
import com.softserveinc.ita.multigame.model.managers.GameManagerImpl;
import com.softserveinc.ita.multigame.model.seabattle.Player;

import java.util.List;

public class ShowGamesService {
    private GameManager gameManager = GameManagerImpl.getInstance();

    public List<Long> getCreatedGamesIds(Player player){
        return  gameManager.getCreatedGamesIds(player);
    }
    public List<Long> getCurrentGamesIds(Player player){
        return  gameManager.getCurrentGamesIds(player);
    }
    public List<Long> getWaitingGamesIds(Player player){
        return  gameManager.getWaitingGamesIds(player);
    }
}
