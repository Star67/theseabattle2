<%@ page import="com.softserveinc.ita.multigame.model.engine.GameResultCode" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Game Info</title>

  <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet'  type='text/css'>
 <%-- <link href="${pageContext.request.contextPath}/resources/css/gameinfo.css" rel="stylesheet">--%>
  <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
  <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/img/target.ico" width="16" height="16" type="image/x-icon">

  <script src="${pageContext.request.contextPath}/resources/js/jquery-3.1.1.js"></script>
  <script src="${pageContext.request.contextPath}/resources/js/gameinfo_2.js"></script>
  <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>

  <link href="${pageContext.request.contextPath}/resources/css/gameinfo.css" rel="stylesheet">
</head>

<body>
<div class="container-fluid bs-cont">
  <!--Top Block-->
  <div class="row bs-row">
    <div class="col-md-4 bs-col-top">
      <ul class="nav nav-pills">
        <li><a href="login"><span class="glyphicon glyphicon-list">&nbsp;</span>Game List</a></li>
        <li><a href="#"><span class="glyphicon glyphicon-home">&nbsp;</span>Profile</a></li>
        <li class="active"><a href="info?id=${param.id}&code=0">GAME</a></li>
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="true">Information<span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#">Rules</a></li>
            <li><a href="#">Policy</a></li>
            <li><a href="#">Partners</a></li>
            <li class="divider"></li>
            <li><a href="#">About author</a></li>
          </ul>
        </li>
      </ul>
    </div>
    <div class="col-md-4 bs-col-top">
      <h1>Game # ${param.id} !</h1>
      <h2>${firstPlayer} vs. ${secondPlayer}</h2>
      <h3>(You're ${player.getLogin()})</h3>
    </div>
    <div class="col-md-4 bs-col-top"></div>
  </div><!--End Of Top Block-->

  <input type="hidden" id="idOfGame" value="${param.id}"/>
  <!--Middle Block-->
  <div class="row bs-row-main">
      <div class="col-md-6 bs-col-c">
          <h4>Your field</h4>
          <!-- Field -->
          <div class="jumbotron jumbo-c" id="board1"></div><!-- End of Field -->
      </div>

      <div class="col-md-6 bs-col-c">
          <h4>Enemy field</h4>
          <!-- Score Field -->
          <div class="jumbotron jumbo-c" id="board2"></div><!-- End of Score Field -->
      </div>

  </div><!--End Of Middle Block-->

  <br/>

  <!--Bot Block-->
  <div class="row bs-row">
    <!--Refresh Button-->
    <div class="col-md-4 bs-col">
      <a href="info?id=${param.id}&code=0"><button type="button" class="btn btn-info"><span class="glyphicon glyphicon-refresh">&nbsp;</span>Refresh</button></a>
    </div><!--End of Refresh Button-->


    <div class="col-md-4 bs-col" id="botBar">
      <!--Make Turn Button-->
      <form class="form-inline">
        <div class="form-group">
          <input id="turnString" type="text" name="turn" class="form-control" placeholder="Enter your hit"/>
          <button id="makeTurn" type="button" onclick="refreshBoard2()" class="btn btn-default"><span class="glyphicon glyphicon-ok">&nbsp;</span>Make Turn</button>
        </div>
     </form><!--End of Make Turn Button-->

      <br/>

      <!--Warnings-->

     <!--End of Warnings-->
    </div>

    <!--Button & Modal-->
    <div class="col-md-4 bs-col">
      <!--Button-->
      <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal3"><span class="glyphicon glyphicon-chevron-left">&nbsp;</span>Back to list</button>
      <!--Modal-->
      <div class="modal fade" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="myModalLabel">Are you sure?</h4>
            </div>
            <div class="modal-body">
              Click OK if you want to close this page and go back to games' list
            </div>
            <div class="modal-footer">
              <a href="login"><button type="submit" class="btn btn-primary">OK</button></a>
              <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
          </div>
        </div>
      </div>

    </div> <!--end Button & Modal-->
  </div><!--End Of Bot Block-->

</div>

</body>
</html>
