<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Game List</title>

        <link href="${pageContext.request.contextPath}/resources/css/gamelist.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/img/target.ico" width="16" height="16" type="image/x-icon">
    </head>

    <body>

        <div class="container-fluid bs-cont">
            <!--Nav Bar, Hello title-->
            <div class="row bs-row">
                <div class="col-md-4 bs-col-top">
                    <ul class="nav nav-pills">
                        <li class="active"><a href="login"><span class="glyphicon glyphicon-list">&nbsp;</span>Game List</a></li>
                        <li><a href="profile"><span class="glyphicon glyphicon-home">&nbsp;</span>Profile</a></li>
                        <li class="disabled"><a href="#">GAME</a></li>
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="true">Information<span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Rules</a></li>
                                    <li><a href="players">Players</a></li>
                                    <li><a href="#">Partners</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#">About author</a></li>
                                </ul>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4 bs-col-top"><h1>Hello, ${player.getLogin()} !</h1></div>
                <div class="col-md-4 bs-col-top">
                    <a href="logout"><button type="button" class="btn btn-success"><span class="glyphicon glyphicon-log-out">&nbsp;</span>Logout</button></a>
                </div>
            </div>

            <!--New game button-->
            <div class="row bs-row">
                <div class="col-md-1 bs-col-top"></div>
                <!--Button & Modal-->
                <div class="col-md-2 bs-col-top">
                    <br/>
                    <br/>
                    <!--Button-->
                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-plus-sign">&nbsp;</span>New game!</button>
                    <!--Modal-->
                    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel">Add new game</h4>
                                </div>
                                <div class="modal-body">
                                    You're going to create a new game as a first player. Do you want default emplacement of ships or custom one?
                                </div>
                                <div class="modal-footer">
                                    <%--<form action="new" method="post">--%>
                                        <div class="form-group">
                                           <%-- <button type="submit" class="btn btn-primary">Default</button>--%>
                                            <a href="new"><button type="button" class="btn btn-primary">Default</button></a>
                                            <a href="create"><button type="button" class="btn btn-default">Personal</button></a>
                                        </div>
                                    <%--</form>--%>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!--end Button & Modal-->

                <div class="col-md-1 bs-col-top"></div>
                <div class="col-md-4 bs-col-top"></div>
            </div>

            <!--Lists of games-->
            <div class="row">
                <div class="col-md-4 bs-col">
                    <div class="list-group">
                        <h3>Created games</h3>
                            <c:forEach var="gameId" items="${createdGames}">
                                <a href="#" class="list-group-item" data-toggle="popover"
                                   data-trigger="hover"
                                   data-placement="top"
                                   data-content="Wait till someone connects!"><c:out value="Game #${gameId}"/></a>
                            </c:forEach>
                    </div>
                </div>

                <div class="col-md-4 bs-col">
                    <div class="list-group">
                        <h3>Current games</h3>
                            <c:forEach var="gameId" items="${currentGames}">
                                <a href="info?id=${gameId}&code=0" class="list-group-item" data-toggle="popover"
                                   data-trigger="hover"
                                   data-placement="top"
                                   data-content="Game in progress"> <c:out value="Game #${gameId}"/> </a>
                            </c:forEach>
                    </div>
                </div>


                <div class="col-md-4 bs-col">
                    <div class="list-group">
                        <h3>Waiting games</h3>
                        <c:forEach var="gameId" items="${waitingGames}">
                            <a href="join?id=${gameId}" class="list-group-item" data-toggle="popover"
                               data-trigger="hover"
                               data-placement="top"
                               data-content="Click to join!"><c:out value="Game #${gameId}"/></a>
                        </c:forEach>
                    </div>
                </div>
            </div>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
        <script>
            $(function () {
                $('[data-toggle="popover"]').popover()
            })
        </script>
    </body>
</html>
