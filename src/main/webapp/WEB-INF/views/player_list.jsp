<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <title>List of players</title>
</head>
<body>
<h1>List of all sea dogs</h1>

<c:forEach var="player" items="${playerList}">
    <a href="#"><c:out value="${player.login}"/></a>
    <br/>
</c:forEach>
</body>
</html>
