<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false" %>
<!DOCTYPE html>
<html>
<head>
  <title>Set ships</title>

  <link href="${pageContext.request.contextPath}/resources/css/set.css" rel="stylesheet">
  <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
  <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/img/target.ico" width="16" height="16" type="image/x-icon">

  <script src="${pageContext.request.contextPath}/resources/js/jquery-3.1.1.js"></script>
  <script src="${pageContext.request.contextPath}/resources/js/setships_2.js"></script>
  <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>

</head>
<body>

<div class="container-fluid bs-cont">

  <div class="row bs-row">
    <div class="col-md-4 bs-col-top">
      <ul class="nav nav-pills">
        <li class="disabled"><a href="#"><span class="glyphicon glyphicon-list">&nbsp;</span>Game List</a></li>
        <li><a href="#"><span class="glyphicon glyphicon-home">&nbsp;</span>Profile</a></li>
        <li class="active"><a href="#">GAME</a></li>
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="true">Information<span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#">Rules</a></li>
            <li><a href="#">Policy</a></li>
            <li><a href="#">Partners</a></li>
            <li class="divider"></li>
            <li><a href="#">About author</a></li>
          </ul>
        </li>
      </ul>
    </div>
    <div class="col-md-4 bs-col-top"><h1>You're daredevil, ${player.login} !</h1>
      <h3>Let's set your ships! (game id = ${id})</h3>
    </div>
    <div class="col-md-4 bs-col-top"></div>
  </div>

  <div class="row bs-row-c">
    <div class="col-md-2 bs-col">

    </div>

    <div class="col-md-6 bs-col">
      <div class="jumbotron jumbo-c" id="board">
        <!-- Digits -->
        <!-- End of Digits -->

        <!-- Field -->
        <!-- End of Field -->
      </div>
    </div>

    <div class="col-md-3 bs-col">
      <div class="jumbotron jumbo-c-r">

          <div class="form-group">
            <input type="hidden" class="form-control" name="id" value="">
            <input type="hidden" class="form-control" name="playerOnPage" value="">
            <input type="text" class="form-control" name="ship" id="ship1" placeholder="Place">

          </div>
          <button type="submit" class="btn btn-success" onclick="add()"><span class="glyphicon glyphicon-plus">&nbsp;</span>Add</button>
        <br/><br/>
          <div class="form-group">
            <input type="hidden" class="form-control" name="id" value="">
            <input type="hidden" class="form-control" name="playerOnPage" value="">
            <input type="text" class="form-control" name="ship" id="ship2" placeholder="Delete">
          </div>
          <button type="submit" class="btn btn-danger" onclick="remove()"><span class="glyphicon glyphicon-minus">&nbsp;</span>Remove</button>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <input type="hidden" id="idOfGame" value="${id}"/>
        <button type="submit" class="btn btn-info" onclick="confirm()"><span class="glyphicon glyphicon-saved">&nbsp;</span>Confirm</button>
        <br/>
        <a href="default?id=${id}"><button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-wrench">&nbsp;</span>Default</button></a>

      </div>
    </div>

    <div class="col-md-1 bs-col">
    </div>
  </div>

</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
</body>
</html>