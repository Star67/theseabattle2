<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Winner</title>
  <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet'  type='text/css'>
  <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
  <link href="${pageContext.request.contextPath}/resources/css/losepage.css" rel="stylesheet">
  <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/img/target.ico" width="16" height="16" type="image/x-icon">
</head>

<body>

<div class="container-fluid bs-cont">
  <div class="row bs-row">
    <div class="col-md-4 bs-col-top">
      <ul class="nav nav-pills">
        <li><a href="login"><span class="glyphicon glyphicon-list">&nbsp;</span>Game List</a></li>
        <li><a href="#"><span class="glyphicon glyphicon-home">&nbsp;</span>Profile</a></li>
        <li class="active"><a href="#">GAME</a></li>
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="true">Information<span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#">Rules</a></li>
            <li><a href="#">Policy</a></li>
            <li><a href="#">Partners</a></li>
            <li class="divider"></li>
            <li><a href="#">About author</a></li>
          </ul>
        </li>
      </ul>
    </div>
  </div>

  <div class="row bs-row-main">
    <div class="col-md-2 bs-col"></div>

    <div class="col-md-8 bs-col">
      <div id='div1' align='center' style='display: none; position:relative;'>
        <div class="jumbotron">
          <h1>Congratulations!</h1>
          <h2>You win!</h2>
          <br/>
          <p><a class="btn btn-primary btn-lg" href="login" role="button">Back to list</a></p>
        </div>
      </div>
    </div>

    <div class="col-md-2 bs-col"></div>

  </div>
</div>




</div>





<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
<script>
  $(document).ready(function() {
    setTimeout("$('#div1').fadeIn('fast');", 1000);
    setTimeout("$('#div1').fadeOut('slow');", 5000);
  });
</script>

</body>
</html>
