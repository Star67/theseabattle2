$(document).ready(function() {
    refreshBoard1();
    printBoard2();
    setInterval(function() {
        refreshBoard1()
    }, 3000);
});

function refreshBoard1(){
    $.ajax({
        method: 'GET',
        url: 'refresh?id=' + $('#idOfGame').val(),
        dataType: 'json',
        success: function (data) {
            if(data == 'Game Over'){
                document.location.href='result?id='+$('#idOfGame').val();
            }
            var letters = [ 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'];
            $('#board1').empty();

            $('#board1').append('<h2><img class="target" src="resources/img/target.png" width="40" height="40"/>'+
                                '<span class="digit">1</span>' +
                                '<span class="digit">2</span> ' +
                                '<span class="digit">3</span>' +
                                '<span class="digit">4</span> ' +
                                '<span class="digit">5</span> ' +
                                '<span class="digit">6</span> ' +
                                '<span class="digit">7</span> ' +
                                '<span class="digit">8</span> ' +
                                '<span class="digit">9</span> ' +
                                '<span class="digit">10</span></h2>');

            $.each(data, function(index, element){
                switch (letters[index]){
                    case 'A': $('#board1').append('<span class="letter" style="font-size: 26px;">A</span>'); break;
                    case 'B': $('#board1').append('<span class="letter" style="font-size: 26px;">B</span>'); break;
                    case 'C': $('#board1').append('<span class="letter" style="font-size: 26px;">C</span>'); break;
                    case 'D': $('#board1').append('<span class="letter" style="font-size: 26px;">D</span>'); break;
                    case 'E': $('#board1').append('<span class="letter" style="font-size: 26px;">E</span>'); break;
                    case 'F': $('#board1').append('<span class="letter" style="font-size: 26px;">F</span>'); break;
                    case 'G': $('#board1').append('<span class="letter" style="font-size: 26px;">G</span>'); break;
                    case 'H': $('#board1').append('<span class="letter" style="font-size: 26px;">H</span>'); break;
                    case 'I': $('#board1').append('<span class="letter" style="font-size: 26px;">I</span>'); break;
                    case 'J': $('#board1').append('<span class="letter" style="font-size: 26px;">J</span>'); break;
                }
                $.each(element, function(ind, elem){
                    switch(elem){
                        case '#': $('#board1').append('<img class="celly" src="resources/img/ship2.png" width="40" height="40"/>'); break;
                        case '_': $('#board1').append('<img class="celly" src="resources/img/anchor2.png" width="40" height="40"/>'); break;
                        case 'X': $('#board1').append('<img class="celly" src="resources/img/hit.png" width="40" height="40"/>'); break;
                        case '*': $('#board1').append('<img class="celly" src="resources/img/miss.png" width="40" height="40"/>'); break;
                    }
                })
                $('#board1').append('<br/>');
            });
        },
        error: function () {
            alert('Error while printing FIELD!');
        }
    });
}

function refreshBoard2(){
    $.ajax({
        method: 'GET',
        url: 'turn?id=' + $('#idOfGame').val() + '&turn=' + $('#turnString').val(),
        dataType: 'json',
        success: function (data2) {
            if(data2 == 'Game Over'){
                printBoard2();
                document.location.href='result?id='+$('#idOfGame').val();
            }

            if (data2 == '4'){
                $('#botBar').append('<div class="alert alert-dismissible alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>It is not your turn!</div>');
                printBoard2();
            }
            if (data2 == '5'){
                $('#botBar').append('<div class="alert alert-dismissible alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>Do not use wrong syntax! Only a-j (A-J) and 1-10 allowed!</div>');
                printBoard2();
            }
            if (data2 == '6'){
                $('#botBar').append('<div class="alert alert-dismissible alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>You have already tried to hit this place!</div>');
                printBoard2();
            }

            var letters = [ 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'];
            $('#board2').empty();

            $('#board2').append('<h2><img class="target" src="resources/img/target.png" width="40" height="40"/>'+
                '<span class="digit">1</span>' +
                '<span class="digit">2</span> ' +
                '<span class="digit">3</span>' +
                '<span class="digit">4</span> ' +
                '<span class="digit">5</span> ' +
                '<span class="digit">6</span> ' +
                '<span class="digit">7</span> ' +
                '<span class="digit">8</span> ' +
                '<span class="digit">9</span> ' +
                '<span class="digit">10</span></h2>');

            $.each(data2, function(index, element){
                switch (letters[index]){
                    case 'A': $('#board2').append('<span class="letter" style="font-size: 26px;">A</span>'); break;
                    case 'B': $('#board2').append('<span class="letter" style="font-size: 26px;">B</span>'); break;
                    case 'C': $('#board2').append('<span class="letter" style="font-size: 26px;">C</span>'); break;
                    case 'D': $('#board2').append('<span class="letter" style="font-size: 26px;">D</span>'); break;
                    case 'E': $('#board2').append('<span class="letter" style="font-size: 26px;">E</span>'); break;
                    case 'F': $('#board2').append('<span class="letter" style="font-size: 26px;">F</span>'); break;
                    case 'G': $('#board2').append('<span class="letter" style="font-size: 26px;">G</span>'); break;
                    case 'H': $('#board2').append('<span class="letter" style="font-size: 26px;">H</span>'); break;
                    case 'I': $('#board2').append('<span class="letter" style="font-size: 26px;">I</span>'); break;
                    case 'J': $('#board2').append('<span class="letter" style="font-size: 26px;">J</span>'); break;
                }
                $.each(element, function(ind, elem){
                    switch(elem){
                        case '#': $('#board2').append('<img class="celly" src="resources/img/ship2.png" width="40" height="40"/>'); break;
                        case '_': $('#board2').append('<img class="celly" src="resources/img/anchor2.png" width="40" height="40"/>'); break;
                        case 'X': $('#board2').append('<img class="celly" src="resources/img/hit.png" width="40" height="40"/>'); break;
                        case '*': $('#board2').append('<img class="celly" src="resources/img/miss.png" width="40" height="40"/>'); break;
                    }
                })
                $('#board2').append('<br/>');
            });

        },
        error: function () {
            alert("Error while printing SCORE!!!");
        }
    });
}

function printBoard2(){
    $.ajax({
        method: 'GET',
        url: 'update?id=' + $('#idOfGame').val(),
        dataType: 'json',
        success: function (data3) {
            if(data3 == 'Game Over'){
                  document.location.href='result?id='+$('#idOfGame').val();
            }
            var letters = [ 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'];
            $('#board2').empty();

            $('#board2').append('<h2><img class="target" src="resources/img/target.png" width="40" height="40"/>'+
                '<span class="digit">1</span>' +
                '<span class="digit">2</span> ' +
                '<span class="digit">3</span>' +
                '<span class="digit">4</span> ' +
                '<span class="digit">5</span> ' +
                '<span class="digit">6</span> ' +
                '<span class="digit">7</span> ' +
                '<span class="digit">8</span> ' +
                '<span class="digit">9</span> ' +
                '<span class="digit">10</span></h2>');

            $.each(data3, function(index, element){
                switch (letters[index]){
                    case 'A': $('#board2').append('<span class="letter" style="font-size: 26px;">A</span>'); break;
                    case 'B': $('#board2').append('<span class="letter" style="font-size: 26px;">B</span>'); break;
                    case 'C': $('#board2').append('<span class="letter" style="font-size: 26px;">C</span>'); break;
                    case 'D': $('#board2').append('<span class="letter" style="font-size: 26px;">D</span>'); break;
                    case 'E': $('#board2').append('<span class="letter" style="font-size: 26px;">E</span>'); break;
                    case 'F': $('#board2').append('<span class="letter" style="font-size: 26px;">F</span>'); break;
                    case 'G': $('#board2').append('<span class="letter" style="font-size: 26px;">G</span>'); break;
                    case 'H': $('#board2').append('<span class="letter" style="font-size: 26px;">H</span>'); break;
                    case 'I': $('#board2').append('<span class="letter" style="font-size: 26px;">I</span>'); break;
                    case 'J': $('#board2').append('<span class="letter" style="font-size: 26px;">J</span>'); break;
                }
                $.each(element, function(ind, elem){
                    switch(elem){
                        case '#': $('#board2').append('<img class="celly" src="resources/img/ship2.png" width="40" height="40"/>'); break;
                        case '_': $('#board2').append('<img class="celly" src="resources/img/anchor2.png" width="40" height="40"/>'); break;
                        case 'X': $('#board2').append('<img class="celly" src="resources/img/hit.png" width="40" height="40"/>'); break;
                        case '*': $('#board2').append('<img class="celly" src="resources/img/miss.png" width="40" height="40"/>'); break;
                    }
                })
                $('#board2').append('<br/>');
            });

        },
        error: function () {
            alert("Error while printing SCORE  for first time!!!");
        }
    });
}
