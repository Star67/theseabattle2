$(document).ready(function() {
    printBoard();
});
var Field;
function printBoard(){
    $.ajax({
        method: 'GET',
        url: 'print',
        dataType: 'json',
        success: function (data) {
            Field = data;
            var letters = [ 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'];
            $('#board').empty();
            $('#board').append('<h2><img class="target" src="resources/img/target.png" width="40" height="40"/>'+
                '<span class="digit">1</span>' +
                '<span class="digit">2</span> ' +
                '<span class="digit">3</span>' +
                '<span class="digit">4</span> ' +
                '<span class="digit">5</span> ' +
                '<span class="digit">6</span> ' +
                '<span class="digit">7</span> ' +
                '<span class="digit">8</span> ' +
                '<span class="digit">9</span> ' +
                '<span class="digit">10</span></h2>');

            $.each(data, function(index, element){
                switch (letters[index]){
                    case 'A': $('#board').append('<span class="letter" style="font-size: 26px;">A</span>'); break;
                    case 'B': $('#board').append('<span class="letter" style="font-size: 26px;">B</span>'); break;
                    case 'C': $('#board').append('<span class="letter" style="font-size: 26px;">C</span>'); break;
                    case 'D': $('#board').append('<span class="letter" style="font-size: 26px;">D</span>'); break;
                    case 'E': $('#board').append('<span class="letter" style="font-size: 26px;">E</span>'); break;
                    case 'F': $('#board').append('<span class="letter" style="font-size: 26px;">F</span>'); break;
                    case 'G': $('#board').append('<span class="letter" style="font-size: 26px;">G</span>'); break;
                    case 'H': $('#board').append('<span class="letter" style="font-size: 26px;">H</span>'); break;
                    case 'I': $('#board').append('<span class="letter" style="font-size: 26px;">I</span>'); break;
                    case 'J': $('#board').append('<span class="letter" style="font-size: 26px;">J</span>'); break;
                }
                $.each(element, function(ind, elem){
                    switch(elem){
                        case '#': $('#board').append('<img class="celly" src="resources/img/ship2.png" width="40" height="40"/>'); break;
                        case '_': $('#board').append('<img class="celly" src="resources/img/anchor2.png" width="40" height="40"/>'); break;
                        case 'X': $('#board').append('<img class="celly" src="resources/img/hit.png" width="40" height="40"/>'); break;
                        case '*': $('#board').append('<img class="celly" src="resources/img/miss.png" width="40" height="40"/>'); break;
                    }
                })
                $('#board').append('<br/>');
            });

        },
        error: function () {
            alert("Error while printing SCORE  for first time!!!");
        }
    });
}

function confirm(){
    var info = {
        field: JSON.stringify(Field),
        id: $('#idOfGame').val()
    }
    $.ajax({
        method: 'POST',
        url: 'confirm',
        dataType: 'json',
        data: info,
        success: function (data2) {
           if(data2 == "field is setted"){
               document.location.href='login';
           }
            else{
               alert("It's impossible to place ships this way - replace or choose DEFAULT!");
           }
        },
        error: function () {
            alert("Error!!!");
        }
    });
}

function add(){
    var jsonToSend = {
        field: JSON.stringify(Field),
        id: $('#idOfGame').val(),
        ship: $('#ship1').val()
    }
    $.ajax({
        method: 'POST',
        url: 'add',
        dataType: 'json',
        data: jsonToSend,
        success: function (data3) {
            if(data3 == "Wrong syntax"){
                alert('Wrong Add Syntax!');
            }
            else {
                Field = data3;
                var letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'];
                $('#board').empty();
                $('#board').append('<h2><img class="target" src="resources/img/target.png" width="40" height="40"/>' +
                    '<span class="digit">1</span>' +
                    '<span class="digit">2</span> ' +
                    '<span class="digit">3</span>' +
                    '<span class="digit">4</span> ' +
                    '<span class="digit">5</span> ' +
                    '<span class="digit">6</span> ' +
                    '<span class="digit">7</span> ' +
                    '<span class="digit">8</span> ' +
                    '<span class="digit">9</span> ' +
                    '<span class="digit">10</span></h2>');

                $.each(data3, function (index, element) {
                    switch (letters[index]) {
                        case 'A':
                            $('#board').append('<span class="letter" style="font-size: 26px;">A</span>');
                            break;
                        case 'B':
                            $('#board').append('<span class="letter" style="font-size: 26px;">B</span>');
                            break;
                        case 'C':
                            $('#board').append('<span class="letter" style="font-size: 26px;">C</span>');
                            break;
                        case 'D':
                            $('#board').append('<span class="letter" style="font-size: 26px;">D</span>');
                            break;
                        case 'E':
                            $('#board').append('<span class="letter" style="font-size: 26px;">E</span>');
                            break;
                        case 'F':
                            $('#board').append('<span class="letter" style="font-size: 26px;">F</span>');
                            break;
                        case 'G':
                            $('#board').append('<span class="letter" style="font-size: 26px;">G</span>');
                            break;
                        case 'H':
                            $('#board').append('<span class="letter" style="font-size: 26px;">H</span>');
                            break;
                        case 'I':
                            $('#board').append('<span class="letter" style="font-size: 26px;">I</span>');
                            break;
                        case 'J':
                            $('#board').append('<span class="letter" style="font-size: 26px;">J</span>');
                            break;
                    }
                    $.each(element, function (ind, elem) {
                        switch (elem) {
                            case '#':
                                $('#board').append('<img class="celly" src="resources/img/ship2.png" width="40" height="40"/>');
                                break;
                            case '_':
                                $('#board').append('<img class="celly" src="resources/img/anchor2.png" width="40" height="40"/>');
                                break;
                            case 'X':
                                $('#board').append('<img class="celly" src="resources/img/hit.png" width="40" height="40"/>');
                                break;
                            case '*':
                                $('#board').append('<img class="celly" src="resources/img/miss.png" width="40" height="40"/>');
                                break;
                        }
                    })
                    $('#board').append('<br/>');
                });
            }

        },
        error: function () {
            alert("Error!!!");
        }
    });

}

function remove(){
    var jsonToSend = {
        field: JSON.stringify(Field),
        id: $('#idOfGame').val(),
        ship: $('#ship2').val()
    }
    $.ajax({
        method: 'POST',
        url: 'remove',
        dataType: 'json',
        data: jsonToSend,
        success: function (data4) {
            if (data4 == "Wrong syntax") {
                alert('Wrong Remove Syntax!');
            }
            else {
                Field = data4;
                var letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'];
                $('#board').empty();
                $('#board').append('<h2><img class="target" src="resources/img/target.png" width="40" height="40"/>' +
                    '<span class="digit">1</span>' +
                    '<span class="digit">2</span> ' +
                    '<span class="digit">3</span>' +
                    '<span class="digit">4</span> ' +
                    '<span class="digit">5</span> ' +
                    '<span class="digit">6</span> ' +
                    '<span class="digit">7</span> ' +
                    '<span class="digit">8</span> ' +
                    '<span class="digit">9</span> ' +
                    '<span class="digit">10</span></h2>');

                $.each(data4, function (index, element) {
                    switch (letters[index]) {
                        case 'A':
                            $('#board').append('<span class="letter" style="font-size: 26px;">A</span>');
                            break;
                        case 'B':
                            $('#board').append('<span class="letter" style="font-size: 26px;">B</span>');
                            break;
                        case 'C':
                            $('#board').append('<span class="letter" style="font-size: 26px;">C</span>');
                            break;
                        case 'D':
                            $('#board').append('<span class="letter" style="font-size: 26px;">D</span>');
                            break;
                        case 'E':
                            $('#board').append('<span class="letter" style="font-size: 26px;">E</span>');
                            break;
                        case 'F':
                            $('#board').append('<span class="letter" style="font-size: 26px;">F</span>');
                            break;
                        case 'G':
                            $('#board').append('<span class="letter" style="font-size: 26px;">G</span>');
                            break;
                        case 'H':
                            $('#board').append('<span class="letter" style="font-size: 26px;">H</span>');
                            break;
                        case 'I':
                            $('#board').append('<span class="letter" style="font-size: 26px;">I</span>');
                            break;
                        case 'J':
                            $('#board').append('<span class="letter" style="font-size: 26px;">J</span>');
                            break;
                    }
                    $.each(element, function (ind, elem) {
                        switch (elem) {
                            case '#':
                                $('#board').append('<img class="celly" src="resources/img/ship2.png" width="40" height="40"/>');
                                break;
                            case '_':
                                $('#board').append('<img class="celly" src="resources/img/anchor2.png" width="40" height="40"/>');
                                break;
                            case 'X':
                                $('#board').append('<img class="celly" src="resources/img/hit.png" width="40" height="40"/>');
                                break;
                            case '*':
                                $('#board').append('<img class="celly" src="resources/img/miss.png" width="40" height="40"/>');
                                break;
                        }
                    })
                    $('#board').append('<br/>');
                });
            }
        }
            ,
        error: function () {
            alert("Error!!!");
        }
    });
}