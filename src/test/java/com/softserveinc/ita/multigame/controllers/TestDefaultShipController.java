package com.softserveinc.ita.multigame.controllers;


import com.softserveinc.ita.multigame.model.seabattle.Player;
import com.softserveinc.ita.multigame.model.seabattle.SeaBattle;
import com.softserveinc.ita.multigame.services.CrudService;
import org.junit.Before;
import org.junit.Test;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.IOException;

import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.*;

public class TestDefaultShipController {
    DefaultShipsController defaultShipsController = new DefaultShipsController();

    HttpServletRequest request;
    HttpServletResponse response;
    HttpSession httpSession;
    SeaBattle game;
    Player player;

    @Before
    public void setUp(){
        request = mock(HttpServletRequest.class);
        response = mock(HttpServletResponse.class);
        httpSession = mock(HttpSession.class);
        defaultShipsController.crudService = mock(CrudService.class);
        game = mock(SeaBattle.class);
        player = new Player("Ivan");
    }

    @Test
    public void isRedirectedOnListPage() throws ServletException, IOException {

        when(request.getParameter("id")).thenReturn("1");
        when(defaultShipsController.crudService.getGame(anyLong())).thenReturn(game);
        when(request.getSession()).thenReturn(httpSession);
        when(httpSession.getAttribute("player")).thenReturn(player);
        when(request.getContextPath()).thenReturn("");

        defaultShipsController.doGet(request, response);

        verify(response).sendRedirect("/login");
        verifyNoMoreInteractions(response);

    }
}
