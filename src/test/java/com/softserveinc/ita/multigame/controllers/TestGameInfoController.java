package com.softserveinc.ita.multigame.controllers;

import com.softserveinc.ita.multigame.model.seabattle.SeaBattle;
import com.softserveinc.ita.multigame.services.CrudService;
import org.junit.Before;
import org.junit.Test;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static org.mockito.Mockito.*;

public class TestGameInfoController {
    GameInfoController gameInfoController;
    HttpServletRequest request;
    HttpServletResponse response;
    RequestDispatcher requestDispatcher;
    SeaBattle seaBattle;

    @Before
    public void setUp(){
        gameInfoController = new GameInfoController();
        gameInfoController.crudService = mock(CrudService.class);
        request = mock(HttpServletRequest.class);
        response = mock(HttpServletResponse.class);
        requestDispatcher = mock(RequestDispatcher.class);
        seaBattle = mock(SeaBattle.class);
    }

    @Test
    public void testIsRedirectResultIfFinished() throws IOException, ServletException {

        when(gameInfoController.crudService.getGame(anyLong())).thenReturn(seaBattle);
        when(seaBattle.isFinished()).thenReturn(true);
        when(request.getParameter("id")).thenReturn("0");
        when(request.getParameter("playerOnPage")).thenReturn("Ivan");
        when(request.getParameter("code")).thenReturn("0");
        when(request.getContextPath()).thenReturn("");

        gameInfoController.doGet(request, response);

        verify(response).sendRedirect(String.format("/result?id=%s", "0"));

    }
    @Test
    public void testIsDispatchedGameinfoIfNotFinished() throws ServletException, IOException {

        when(request.getRequestDispatcher(anyString())).thenReturn(requestDispatcher);
        when(gameInfoController.crudService.getGame(anyLong())).thenReturn(seaBattle);
        when(seaBattle.isFinished()).thenReturn(false);
        when(request.getParameter("id")).thenReturn("0");
        when(request.getParameter("playerOnPage")).thenReturn("Ivan");
        when(request.getParameter("code")).thenReturn("0");

        gameInfoController.doGet(request, response);

        verify(requestDispatcher).forward(request, response);
    }


}
