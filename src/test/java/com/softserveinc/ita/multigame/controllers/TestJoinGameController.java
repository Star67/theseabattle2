package com.softserveinc.ita.multigame.controllers;


import org.junit.Before;
import org.junit.Test;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

import static org.mockito.Mockito.*;

public class TestJoinGameController {
    JoinGameController joinGameController = new JoinGameController();

    HttpServletRequest request;
    HttpServletResponse response;
    RequestDispatcher requestDispatcher;

    @Before
    public void setUp(){
        request = mock(HttpServletRequest.class);
        response = mock(HttpServletResponse.class);
        requestDispatcher = mock(RequestDispatcher.class);
    }

    @Test
    public void testJoining() throws ServletException, IOException {

        when(request.getParameter("id")).thenReturn("1");
        when(request.getRequestDispatcher(anyString())).thenReturn(requestDispatcher);

        joinGameController.doGet(request, response);

        verify(request).getParameter("id");
        verify(request).setAttribute("id", 1L);
        verify(request).getRequestDispatcher("WEB-INF/views/setships_2.jsp");
        verifyNoMoreInteractions(request);
        verify(requestDispatcher).forward(request, response);
        verifyNoMoreInteractions(requestDispatcher);
    }
}
