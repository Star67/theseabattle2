package com.softserveinc.ita.multigame.controllers;


import com.softserveinc.ita.multigame.model.seabattle.Player;
import com.softserveinc.ita.multigame.services.PlayerService;
import com.softserveinc.ita.multigame.services.ShowGamesService;
import org.junit.Before;
import org.junit.Test;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Arrays;

import static org.mockito.Mockito.*;

public class TestLoginController {
    LoginController loginController;
    @Before
    public void setUp(){
        loginController = new LoginController();
        loginController.playerService = mock(PlayerService.class);
        loginController.showGamesService = mock(ShowGamesService.class);
    }

    @Test
    public void testLogging() throws ServletException, IOException {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        HttpSession httpSession = mock(HttpSession.class);
        RequestDispatcher requestDispatcher = mock(RequestDispatcher.class);
        Player player = new Player("Ivan");


        when(request.getRemoteUser()).thenReturn("Ivan");
        when(loginController.playerService.get("Ivan")).thenReturn(player);
        when(loginController.showGamesService.getCreatedGamesIds(player)).thenReturn(Arrays.asList(0l,1l));
        when(loginController.showGamesService.getCurrentGamesIds(player)).thenReturn(Arrays.asList(0l,1l));
        when(loginController.showGamesService.getWaitingGamesIds(player)).thenReturn(Arrays.asList(0l,1l));
        when(request.getSession()).thenReturn(httpSession);
        when(request.getRequestDispatcher("WEB-INF/views/gamelist.jsp")).thenReturn(requestDispatcher);

        loginController.doGet(request, response);

        verify(httpSession).setAttribute("player", player);
        verify(requestDispatcher).forward(request, response);

    }
}
