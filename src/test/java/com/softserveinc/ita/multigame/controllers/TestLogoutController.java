package com.softserveinc.ita.multigame.controllers;


import com.google.gson.Gson;
import org.junit.Before;
import org.junit.Test;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.IOException;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestLogoutController {
    LogoutController logoutController = new LogoutController();
    HttpServletRequest request;
    HttpServletResponse response;
    HttpSession httpSession;


    @Before
    public void setUp(){
        request = mock(HttpServletRequest.class);
        response = mock(HttpServletResponse.class);
        httpSession = mock(HttpSession.class);
    }

    @Test
    public void invalidateSessionAfterLogout() throws ServletException, IOException {

        when(request.getSession()).thenReturn(httpSession);

        logoutController.doGet(request, response);

        verify(httpSession).invalidate();

    }
    @Test
    public void redirectToLoginAfterSessionInvalidation() throws ServletException, IOException {
        when(request.getSession()).thenReturn(httpSession);
        when(request.getContextPath()).thenReturn("");

        logoutController.doGet(request, response);

        verify(response).sendRedirect("/login");
    }
}
