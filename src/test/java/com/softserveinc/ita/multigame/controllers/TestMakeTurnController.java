package com.softserveinc.ita.multigame.controllers;


import com.google.gson.Gson;
import com.softserveinc.ita.multigame.model.seabattle.Player;
import com.softserveinc.ita.multigame.model.seabattle.SeaBattle;
import com.softserveinc.ita.multigame.services.CrudService;
import com.softserveinc.ita.multigame.services.MakeTurnService;
import org.junit.Before;
import org.junit.Test;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.IOException;
import java.io.PrintWriter;

import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestMakeTurnController {
    MakeTurnController makeTurnController = new MakeTurnController();

    HttpServletRequest request;
    HttpServletResponse response;
    HttpSession httpSession;
    PrintWriter printWriter;
    SeaBattle game;
    Player player;

    @Before
    public void setUp(){
        request = mock(HttpServletRequest.class);
        response = mock(HttpServletResponse.class);
        httpSession = mock(HttpSession.class);
        makeTurnController.makeTurnService = mock(MakeTurnService.class);
        makeTurnController.crudService = mock(CrudService.class);
        printWriter = mock(PrintWriter.class);
        game = mock(SeaBattle.class);
        player = new Player("Ivan");
    }

    @Test
    public void isSentGameOverIfGameIsFinished() throws ServletException, IOException {

        when(request.getParameter("id")).thenReturn("1");
        when(request.getSession()).thenReturn(httpSession);
        when(httpSession.getAttribute("player")).thenReturn(player);
        when(makeTurnController.crudService.getGame(anyLong())).thenReturn(game);
        when(game.isFinished()).thenReturn(true);
        when(response.getWriter()).thenReturn(printWriter);

        makeTurnController.doGet(request, response);

        verify(printWriter).write("\"Game Over\"");
    }

    @Test
    public void isSentCodeIfGameHasBadCode() throws ServletException, IOException {

        when(request.getParameter("id")).thenReturn("1");
        when(request.getSession()).thenReturn(httpSession);
        when(httpSession.getAttribute("player")).thenReturn(player);
        when(makeTurnController.crudService.getGame(anyLong())).thenReturn(game);
        when(game.isFinished()).thenReturn(false);
        when(game.getResultCode()).thenReturn(1);
        when(response.getWriter()).thenReturn(printWriter);

        makeTurnController.doGet(request, response);

        verify(printWriter).write(String.valueOf(1));
    }


}
