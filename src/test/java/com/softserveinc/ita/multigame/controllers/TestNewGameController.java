package com.softserveinc.ita.multigame.controllers;

import com.softserveinc.ita.multigame.model.seabattle.Player;
import com.softserveinc.ita.multigame.services.CrudService;
import com.softserveinc.ita.multigame.services.PlayerService;
import org.junit.Before;
import org.junit.Test;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.IOException;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestNewGameController {
    NewGameController newGameController = new NewGameController();

    HttpServletRequest request;
    HttpServletResponse response;
    HttpSession httpSession;

    @Before
    public void setUp(){
        request = mock(HttpServletRequest.class);
        response = mock(HttpServletResponse.class);
        httpSession = mock(HttpSession.class);
        newGameController.playerService = mock(PlayerService.class);
        newGameController.crudService = mock(CrudService.class);
    }

    @Test
    public void isRedirectIfGameCreated() throws ServletException, IOException {

        Player player = new Player("Ivan");

        when(request.getContextPath()).thenReturn("");
        when(request.getSession()).thenReturn(httpSession);
        when(httpSession.getAttribute("player")).thenReturn(player);

        newGameController.doGet(request, response);

        verify(response).sendRedirect("/login");
    }
}
