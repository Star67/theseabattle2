package com.softserveinc.ita.multigame.controllers;

import com.softserveinc.ita.multigame.model.seabattle.Player;
import com.softserveinc.ita.multigame.model.seabattle.SeaBattle;
import com.softserveinc.ita.multigame.services.CrudService;
import org.junit.Before;
import org.junit.Test;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestRefreshFieldController {
    RefreshFieldController refreshFieldController = new RefreshFieldController();

    HttpServletRequest request;
    HttpServletResponse response;
    HttpSession httpSession;
    PrintWriter printWriter;
    SeaBattle game;
    Player player;

    @Before
    public void setUp(){
        request = mock(HttpServletRequest.class);
        response = mock(HttpServletResponse.class);
        httpSession = mock(HttpSession.class);
        refreshFieldController.crudService = mock(CrudService.class);
        printWriter = mock(PrintWriter.class);
        game = mock(SeaBattle.class);
        player = mock(Player.class);
    }

    @Test
    public void isSentGameOverIfGameIsFinished() throws IOException, ServletException {

        when(request.getParameter("id")).thenReturn("1");
        when(request.getSession()).thenReturn(httpSession);
        when(refreshFieldController.crudService.getGame(1L)).thenReturn(game);
        when(httpSession.getAttribute("player")).thenReturn(player);
        when(game.isFinished()).thenReturn(true);

        when(player.getLogin()).thenReturn("Ivan");
        when(game.getFirstPlayer()).thenReturn("Ivan");

        when(response.getWriter()).thenReturn(printWriter);

        refreshFieldController.doGet(request, response);

        verify(printWriter).write("\"Game Over\"");
    }

}
