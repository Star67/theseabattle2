package com.softserveinc.ita.multigame.controllers;


import com.softserveinc.ita.multigame.model.seabattle.Player;
import com.softserveinc.ita.multigame.services.PlayerService;
import org.junit.Before;
import org.junit.Test;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.IOException;
import java.io.PrintWriter;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestRegistrationController {
    RegistrationController registrationController = new RegistrationController();

    HttpServletRequest request;
    HttpServletResponse response;
    HttpSession httpSession;
    private PrintWriter printWriter;

    @Before
    public void setUp(){
        request = mock(HttpServletRequest.class);
        response = mock(HttpServletResponse.class);
        httpSession = mock(HttpSession.class);
        registrationController.playerService = mock(PlayerService.class);
        printWriter = mock(PrintWriter.class);
    }

    @Test
    public void checkIfPlayerAlreadyExists() throws ServletException, IOException {

        when(registrationController.playerService.create(any(Player.class))).thenReturn(0);
        when(response.getWriter()).thenReturn(printWriter);

        registrationController.doPost(request, response);

        verify(printWriter).print("<h1>Seems like this player already exists...</h1>");
    }
    @Test
    public void checkIfPlayerIsNew() throws ServletException, IOException {
        Player player = new Player("1","1", "1");

        when(registrationController.playerService.create(any(Player.class))).thenReturn(1);
        when(request.getParameter("login")).thenReturn("1");
        when(request.getParameter("email")).thenReturn("1");
        when(request.getParameter("password")).thenReturn("1");
        when(request.getContextPath()).thenReturn("");

        registrationController.doPost(request, response);

        verify(registrationController.playerService).addRole(player, "players");
    }

}
