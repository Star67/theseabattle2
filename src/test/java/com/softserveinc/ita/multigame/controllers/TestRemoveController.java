package com.softserveinc.ita.multigame.controllers;

import com.softserveinc.ita.multigame.model.seabattle.SeaBattle;
import com.softserveinc.ita.multigame.services.CrudService;
import org.junit.Before;
import org.junit.Test;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;

import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestRemoveController {
    RemoveController removeController = new RemoveController();

    HttpServletRequest request;
    HttpServletResponse response;
    SeaBattle game;
    PrintWriter printWriter;

    @Before
    public void setUp(){
        request = mock(HttpServletRequest.class);
        response = mock(HttpServletResponse.class);
        removeController.crudService = mock(CrudService.class);
        game = mock(SeaBattle.class);
        printWriter = mock(PrintWriter.class);
    }

    @Test
    public void checkNoRemovingIfWrongSyntax() throws ServletException, IOException {

        String field = "_______________________________________________________________________________________________________________________________________________________________________________";

        when(request.getParameter("field")).thenReturn(field);
        when(request.getParameter("id")).thenReturn("1");
        when(removeController.crudService.getGame(anyLong())).thenReturn(game);
        when(response.getWriter()).thenReturn(printWriter);
        when(game.validateTurnSyntax(anyString())).thenReturn(false);

        removeController.doPost(request, response);

        verify(printWriter).write("\"Wrong syntax\"");

    }
}
