package com.softserveinc.ita.multigame.controllers;

import com.softserveinc.ita.multigame.model.seabattle.Player;
import com.softserveinc.ita.multigame.model.seabattle.SeaBattle;
import com.softserveinc.ita.multigame.services.CrudService;
import com.softserveinc.ita.multigame.services.MakeTurnService;
import org.junit.Before;
import org.junit.Test;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.IOException;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestResultController {
    ResultController resultController = new ResultController();

    HttpServletRequest request;
    HttpServletResponse response;
    HttpSession httpSession;
    RequestDispatcher requestDispatcher;
    SeaBattle game;
    Player player;

    @Before
    public void setUp(){
        request = mock(HttpServletRequest.class);
        response = mock(HttpServletResponse.class);
        httpSession = mock(HttpSession.class);
        requestDispatcher = mock(RequestDispatcher.class);
        game = mock(SeaBattle.class);
        player = new Player("Ivan");
    }

    @Test
    public void isDispatched() throws ServletException, IOException {

        when(request.getParameter("id")).thenReturn("1");
        when(request.getSession()).thenReturn(httpSession);
        when(httpSession.getAttribute("player")).thenReturn(player);
        when(request.getRequestDispatcher(anyString())).thenReturn(requestDispatcher);

        resultController.doGet(request, response);

        verify(requestDispatcher).forward(request, response);
    }
}
