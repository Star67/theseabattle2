package com.softserveinc.ita.multigame.model.seabattle;


import com.softserveinc.ita.multigame.model.engine.GameEngine;
import com.softserveinc.ita.multigame.model.engine.GameResultCode;
import com.softserveinc.ita.multigame.model.engine.GameState;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class TestSeaBattleEngine {
	
	GameEngine game;
	
	@Before
	public void setUp(){
		game = new SeaBattle();
		game.setFirstPlayer("Ivan");
		game.setSecondPlayer("Petro");
	}
	
	@Test
	public void playerHitWithResultCodeOk(){
		
		int expected = GameResultCode.OK;
		
		game.makeTurn(game.getFirstPlayer(), "a1");
		int actual = game.getResultCode();
		
		assertThat(actual, equalTo(expected));
	}
	
	@Test
	public void playerCannotTurnTwiceIfMiss(){
		
		int expected = GameResultCode.BAD_TURN_ORDER;
		
		game.makeTurn(game.getFirstPlayer(), "a5");
		game.makeTurn(game.getFirstPlayer(), "a1");
		int actual = game.getResultCode();
		
		assertThat(actual, equalTo(expected));
	}
	
	@Test
	public void playerMustTurnTwiceIfHit(){
		
		int expected = GameResultCode.OK;
		
		game.makeTurn(game.getFirstPlayer(), "a1");
		game.makeTurn(game.getFirstPlayer(), "a2");
		int actual = game.getResultCode();
		
		assertThat(actual, equalTo(expected));
	}
	
	@Test
	public void cannotMakeTurnWithWrongLetterSyntax(){
		
		int expected = GameResultCode.BAD_TURN_SYNTAX;
		
		game.makeTurn(game.getFirstPlayer(), "k1");
		int actual = game.getResultCode();
		
		assertThat(actual, equalTo(expected));
		
	}
	
	@Test
	public void cannotMakeTurnWithWrongDigitSyntax(){
		
		int expected = GameResultCode.BAD_TURN_SYNTAX;
		
		game.makeTurn(game.getFirstPlayer(), "a0");
		int actual = game.getResultCode();
		
		assertThat(actual, equalTo(expected));
		
	}
	
	@Test
	public void cannotMakeTurnWithWrongLetterAndDigitSyntax(){
		
		int expected = GameResultCode.BAD_TURN_SYNTAX;
		
		game.makeTurn(game.getFirstPlayer(), "z11");
		int actual = game.getResultCode();
		
		assertThat(actual, equalTo(expected));
		
	}
	
	@Test
	public void cannotStartWithoutBothPlayersRegistration(){
		
		int expected = GameResultCode.BAD_TURN_FOR_NOT_STARTED_GAME;
		
		GameEngine game = new SeaBattle();
		game.setFirstPlayer("Ivan");
		game.makeTurn(game.getFirstPlayer(), "a1");
		int actual = game.getResultCode();
		
		assertThat(actual, equalTo(expected));
	}
	
	private void firstPlayerWon(){
		game.makeTurn("Ivan", "a1");
		game.makeTurn("Ivan", "a2");
		game.makeTurn("Ivan", "a3");
		game.makeTurn("Ivan", "a4");
		game.makeTurn("Ivan", "c1");
		game.makeTurn("Ivan", "c2");
		game.makeTurn("Ivan", "c3");
		game.makeTurn("Ivan", "c5");
		game.makeTurn("Ivan", "c6");
		game.makeTurn("Ivan", "c7");
		game.makeTurn("Ivan", "e1");
		game.makeTurn("Ivan", "e2");
		game.makeTurn("Ivan", "e4");
		game.makeTurn("Ivan", "e5");
		game.makeTurn("Ivan", "e7");
		game.makeTurn("Ivan", "e8");
		game.makeTurn("Ivan", "g1");
		game.makeTurn("Ivan", "g3");
		game.makeTurn("Ivan", "g5");
		game.makeTurn("Ivan", "g7");
	}
	
	@Ignore("Architecture troubles")
	@Test
	public void cannotMakeTurnAfterFinishedGame(){
		
		int expected = GameResultCode.BAD_TURN_FOR_FINISHED_GAME;
		
		firstPlayerWon();
		game.makeTurn(game.getFirstPlayer(), "j1");
		int actual = game.getResultCode();
		
		assertThat(actual, equalTo(expected));
		
		//Trouble: we make turn after finished game, and after this turn 
		//game return GameResultCode.BAD_TURN_FOR_NOT_STARTED_GAME, but not GameResultCode.BAD_TURN_FOR_FINISHED_GAME
	}

	@Test
	public void returnTurnToPlayerIfHeHitSamePlace(){
		
		int expected = GameResultCode.OK;
		
		game.makeTurn(game.getFirstPlayer(), "a1");
		game.makeTurn(game.getFirstPlayer(), "a1");
		game.makeTurn(game.getFirstPlayer(), "a2");
		int actual = game.getResultCode();
		
		assertThat(actual, equalTo(expected));
	}
	
	@Test
	public void dontReturnTurnToAnotherPlayerIfPreviousHitSamePlace(){
		
		int expected = GameResultCode.BAD_TURN_ORDER;
		
		game.makeTurn(game.getFirstPlayer(), "a1");
		game.makeTurn(game.getFirstPlayer(), "a1");
		game.makeTurn(game.getSecondPlayer(), "a2");
		int actual = game.getResultCode();
		
		assertThat(actual, equalTo(expected));
	}
	
	@Test
	public void returnTurnToPlayerIfHeMissedSamePlace(){
		
		int expected = GameResultCode.OK;
		
		game.makeTurn(game.getFirstPlayer(), "a5");
		game.makeTurn(game.getSecondPlayer(), "j10");
		game.makeTurn(game.getFirstPlayer(), "a5");
		game.makeTurn(game.getFirstPlayer(), "a2");
		int actual = game.getResultCode();
		
		assertThat(actual, equalTo(expected));
	}
	
	@Test
	public void dontReturnTurnToAnotherPlayerIfPreviousMissedtSamePlace(){
		
		int expected = GameResultCode.BAD_TURN_ORDER;
		
		game.makeTurn(game.getFirstPlayer(), "a5");
		game.makeTurn(game.getSecondPlayer(), "j10");
		game.makeTurn(game.getFirstPlayer(), "a5");
		game.makeTurn(game.getSecondPlayer(), "a1");
		int actual = game.getResultCode();
		
		assertThat(actual, equalTo(expected));
	}
	
	@Test
	public void badSyntaxForTheTurnLongerThan3(){
		int expected = GameResultCode.BAD_TURN_SYNTAX;
		
		game.makeTurn(game.getFirstPlayer(), "aaa5");
		int actual = game.getResultCode();
		
		assertThat(actual, equalTo(expected));
	}
	
	@Test
	public void badSyntaxForTheTurnShorterThan2(){
		int expected = GameResultCode.BAD_TURN_SYNTAX;
		
		game.makeTurn(game.getFirstPlayer(), "");
		int actual = game.getResultCode();
		
		assertThat(actual, equalTo(expected));
	}
	
	@Test
	public void badSyntaxForTheTurnWithWrongParams(){
		int expected = GameResultCode.BAD_TURN_SYNTAX;
		
		game.makeTurn(game.getFirstPlayer(), "_&!");
		int actual = game.getResultCode();
		
		assertThat(actual, equalTo(expected));
	}
	
	@Test
	public void badSyntaxForTheTurnWithWrongParams_2(){
		int expected = GameResultCode.BAD_TURN_SYNTAX;
		
		game.makeTurn(game.getFirstPlayer(), "j0");
		int actual = game.getResultCode();
		
		assertThat(actual, equalTo(expected));
	}
	
	
	@Test
	public void cannotStartIfOnlySecondPlayerRegistrated(){
		
		int expected = GameResultCode.BAD_TURN_FOR_NOT_STARTED_GAME;
		
		GameEngine game = new SeaBattle();
		game.setSecondPlayer("Ivan");
		game.makeTurn(game.getSecondPlayer(), "a1");
		int actual = game.getResultCode();
		
		assertThat(actual, equalTo(expected));
	}
	
	@Test
	public void cannotStartIfSecondPlayerRegistratedBeforeFirstPlayer(){
		
		int expected = GameResultCode.BAD_TURN_FOR_NOT_STARTED_GAME;
		
		GameEngine game = new SeaBattle();
		game.setSecondPlayer("Petro");
		game.setFirstPlayer("Ivan");
		game.makeTurn(game.getFirstPlayer(), "a1");
		int actual = game.getResultCode();
		
		assertThat(actual, equalTo(expected));
	}
	
	@Test
	public void cannotStartWithNullFirstPlayerName(){
		
		int expected = GameResultCode.BAD_PLAYER;
		
		GameEngine game = new SeaBattle();
		game.setFirstPlayer("");
		int actual = game.getResultCode();
		
		assertThat(actual, equalTo(expected));
	}
	
	
	@Test
	public void cannotStartWithNullSecondPlayerName(){
		
		int expected = GameResultCode.BAD_PLAYER;
		
		GameEngine game = new SeaBattle();
		game.setFirstPlayer("Ivan");
	    game.setSecondPlayer("");
		int actual = game.getResultCode();
		
		assertThat(actual, equalTo(expected));
	}
	
	@Test
	public void showPlayerIfHeHitSamePlace(){
		
		int expected = GameResultCode.BAD_TURN_LOGIC;
		
		game.makeTurn(game.getFirstPlayer(), "a1");
		game.makeTurn(game.getFirstPlayer(), "a1");
		int actual = game.getResultCode();
		
		assertThat(actual, equalTo(expected));
	}
	
	@Test
	public void showPlayerIfHeMissedSamePlace(){
		
		int expected = GameResultCode.BAD_TURN_LOGIC;
		
		game.makeTurn(game.getFirstPlayer(), "j10");
		game.makeTurn(game.getSecondPlayer(), "j10");
		game.makeTurn(game.getFirstPlayer(), "J10");
		int actual = game.getResultCode();
		
		assertThat(actual, equalTo(expected));
	}
	
	@Test
	public void cannotSetNameAfterStart(){
		
		int expected = GameResultCode.BAD_FIRST_PLAYER_ORDER;
		
		game.setFirstPlayer("New name");
		int actual = game.getResultCode();
		
		assertThat(actual, equalTo(expected));
	}
}
